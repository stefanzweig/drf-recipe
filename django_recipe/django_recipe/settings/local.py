from .settings import *

DEBUG = True

# SECURITY WARNING: don't run with debug turned on in production!
ALLOWED_HOSTS = ["0.0.0.0", "127.0.0.1"]
