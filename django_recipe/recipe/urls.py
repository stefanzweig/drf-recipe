from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from recipe import views

router = DefaultRouter()
router.register("tags", views.TagViewSet)
router.register("ingredients", views.IngredientViewSet)
router.register("recipes", views.RecipeViewSet)

app_name = "recipe"

urlpatterns = [url(r"^", include(router.urls))]
