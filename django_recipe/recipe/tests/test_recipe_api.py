import tempfile
import os

from PIL import Image

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.shortcuts import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Recipe, Ingredient, Tag
from recipe.serializers import RecipeSerializer, RecipeDetailSerializer


class RecipePublicApiTestCase(TestCase):
    """ Testing public recipe api """

    def setUp(self):
        self.recipe_endpoint = reverse("recipe:recipe-list")
        self.client = APIClient()

    @staticmethod
    def _create_recipe(user, **kwargs):
        """ Create and return a recipe """
        defaults = {"title": "Sample Recipe", "time_in_minutes": 10, "price": 10.00}
        defaults.update(kwargs)
        return Recipe.objects.create(user=user, **defaults)

    def test_authentication_required(self):
        """ Test that user is authenticated """
        response = self.client.get(self.recipe_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class RecipePrivateApiTestCase(TestCase):
    """ Testing private recipe api """

    def setUp(self):
        self.recipe_endpoint = reverse("recipe:recipe-list")
        self.client = APIClient()
        self.email = "test@example.com"
        self.password = "testpass"
        self.user = self._create_user(email=self.email, password=self.password)
        self.client.force_authenticate(self.user)

    @staticmethod
    def recipe_detail_endpoint(recipe_id):
        """ Return recipe detail url """
        return reverse("recipe:recipe-detail", args=[recipe_id])

    @staticmethod
    def _create_tag(**kwargs):
        """ Create and return a taag """
        return Tag.objects.create(**kwargs)

    @staticmethod
    def _create_ingredient(**kwargs):
        """ Create and return a taag """
        return Ingredient.objects.create(**kwargs)

    @staticmethod
    def _create_user(**kwargs):
        user = get_user_model().objects.create_user(**kwargs)
        return user

    @staticmethod
    def _create_recipe(user, **kwargs):
        """ Create and return a recipe """
        defaults = {"title": "Sample Recipe", "time_in_minutes": 10, "price": 10.00}
        defaults.update(kwargs)
        return Recipe.objects.create(user=user, **defaults)

    def test_get_recipes(self):
        """ Test that user is authenticated """
        [self._create_recipe(self.user) for i in range(0, 2)]
        response = self.client.get(self.recipe_endpoint)

        recipes = Recipe.objects.all().order_by("-id")
        serializer = RecipeSerializer(recipes, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_recipe_restricted_to_user(self):
        user2 = self._create_user(email="test2@example.com", password="testpass")
        self._create_recipe(user=self.user)
        self._create_recipe(user=user2)

        response = self.client.get(self.recipe_endpoint)
        recipes = Recipe.objects.filter(user=self.user)
        serializer = RecipeSerializer(recipes, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, serializer.data)

    def test_recipe_detail_page(self):
        """ Test viewing a recipe detail page """
        recipe = self._create_recipe(self.user)
        recipe.tags.add(self._create_tag(name="Cream", user=self.user))
        recipe.ingredients.add(self._create_ingredient(name="Milk", user=self.user))
        detail_endpoint = self.recipe_detail_endpoint(recipe.id)

        response = self.client.get(detail_endpoint)

        serializer = RecipeDetailSerializer(recipe)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, serializer.data)

    def test_create_basic_recipe(self):
        """ Test creating a recipe """
        payload = {"title": "Banana Cake", "time_in_minutes": 30, "price": 5.00}
        response = self.client.post(self.recipe_endpoint, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        recipe = Recipe.objects.get(id=response.data["id"])

        for key in payload.keys():
            self.assertEqual(payload[key], getattr(recipe, key))

    def test_create_recipe_with_tags(self):
        """ Test creating a recipe with tags """
        tag1 = self._create_tag(user=self.user, name="Vegan")
        tag2 = self._create_tag(user=self.user, name="Dessert")
        payload = {
            "title": "Banana Cake",
            "time_in_minutes": 30,
            "price": 5.00,
            "tags": [tag1.id, tag2.id],
        }
        response = self.client.post(self.recipe_endpoint, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        recipe = Recipe.objects.get(id=response.data["id"])
        tags = recipe.tags.all()
        self.assertEqual(len(tags), 2)
        self.assertIn(tag1, tags)
        self.assertIn(tag2, tags)

    def test_create_recipe_with_ingredients(self):
        """ Test creating recipe with ingredients """

        ingredient1 = self._create_ingredient(user=self.user, name="Milk")
        ingredient2 = self._create_ingredient(user=self.user, name="Sugar")
        payload = {
            "title": "Banana Cake",
            "time_in_minutes": 30,
            "price": 5.00,
            "ingredients": [ingredient1.id, ingredient2.id],
        }

        response = self.client.post(self.recipe_endpoint, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        recipe = Recipe.objects.get(id=response.data["id"])
        ingredients = recipe.ingredients.all()
        self.assertEqual(len(ingredients), 2)
        self.assertIn(ingredient1, ingredients)
        self.assertIn(ingredient2, ingredients)

    def test_partial_update_recipe(self):
        """ Test updating a recipe with patch """
        recipe = self._create_recipe(user=self.user)
        recipe.tags.add(self._create_tag(user=self.user, name="Vegan"))
        new_tag = self._create_tag(user=self.user, name="Curry")
        payload = {"title": "Chicken Tikka", "tags": [new_tag.id]}
        url = self.recipe_detail_endpoint(recipe.id)
        self.client.patch(url, payload)

        recipe.refresh_from_db()
        self.assertEqual(recipe.title, payload["title"])
        tags = recipe.tags.all()
        self.assertEqual(len(tags), 1)
        self.assertIn(new_tag, tags)

    def test_full_update_recipe(self):
        recipe = self._create_recipe(user=self.user)
        recipe.tags.add(self._create_tag(user=self.user, name="Apple"))
        payload = {"title": "Apple Cake", "time_in_minutes": 24, "price": 999.00}
        url = self.recipe_detail_endpoint(recipe.id)
        self.client.put(url, payload)

        recipe.refresh_from_db()
        self.assertEqual(recipe.title, payload["title"])
        self.assertEqual(recipe.price, payload["price"])
        self.assertEqual(recipe.time_in_minutes, payload["time_in_minutes"])
        self.assertFalse(recipe.tags.exists())

    def test_filter_recipe_by_tags(self):
        """ Return recipe with specific tag """
        recipe1 = self._create_recipe(user=self.user, title="Thai Veggies")
        recipe2 = self._create_recipe(user=self.user, title="Jollof Rice")
        tag1 = self._create_tag(user=self.user, name="Vegan")
        tag2 = self._create_tag(user=self.user, name="Meat Eater")
        recipe1.tags.add(tag1)
        recipe2.tags.add(tag2)
        recipe3 = self._create_recipe(user=self.user, title="Marmite Cake")

        response = self.client.get(
            self.recipe_endpoint, {"tags": f"{tag1.slug}, {tag2.slug}"}
        )
        serializer1 = RecipeSerializer(recipe1)
        serializer2 = RecipeSerializer(recipe2)
        serializer3 = RecipeSerializer(recipe3)
        self.assertIn(serializer1.data, response.data)
        self.assertIn(serializer2.data, response.data)
        self.assertNotIn(serializer3.data, response.data)

    def test_filter_recipe_by_ingredients(self):
        """ Return recipe with specific ingredients"""
        recipe1 = self._create_recipe(user=self.user, title="Thai Veggies")
        recipe2 = self._create_recipe(user=self.user, title="Jollof Rice")
        ingredient1 = self._create_ingredient(user=self.user, name="Pepper")
        ingredient2 = self._create_ingredient(user=self.user, name="Cucumber")
        recipe3 = self._create_recipe(user=self.user, title="Cyanide and Happiness")
        recipe1.ingredients.add(ingredient1)
        recipe2.ingredients.add(ingredient2)
        response = self.client.get(
            self.recipe_endpoint,
            {"ingredients": f"{ingredient1.slug}, {ingredient2.slug}"},
        )
        serializer1 = RecipeSerializer(recipe1)
        serializer2 = RecipeSerializer(recipe2)
        serializer3 = RecipeSerializer(recipe3)
        self.assertIn(serializer1.data, response.data)
        self.assertIn(serializer2.data, response.data)
        self.assertNotIn(serializer3.data, response.data)


class RecipeImageUploadTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = self._create_user(email="test@example.com", password="testpass")
        self.client.force_authenticate(self.user)
        self.recipe = self._create_recipe(user=self.user)

    def tearDown(self):
        self.user.delete()

    @staticmethod
    def recipe_image_endpoint(recipe_id):
        """ Return recipe image upload url """
        return reverse("recipe:recipe-upload-image", args=[recipe_id])

    @staticmethod
    def _create_user(**kwargs):
        user = get_user_model().objects.create_user(**kwargs)
        return user

    @staticmethod
    def _create_recipe(user, **kwargs):
        """ Create and return a recipe """
        defaults = {"title": "Sample Recipe", "time_in_minutes": 10, "price": 10.00}
        defaults.update(kwargs)
        return Recipe.objects.create(user=user, **defaults)

    def test_upload_recipe_image(self):
        """ Test uploading an image to a recipe """
        url = self.recipe_image_endpoint(self.recipe.id)
        with tempfile.NamedTemporaryFile(suffix=".jpg") as ntf:
            img = Image.new("RGB", (100, 100))
            img.save(ntf, format="JPEG")
            ntf.seek(0)
            response = self.client.post(url, {"image": ntf}, format="multipart")
        self.recipe.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("image", response.data)
        self.assertTrue(os.path.exists(self.recipe.image.path))

    def test_upload_recipe_image_bad_request(self):
        """ Test uploading an invalid image """
        url = self.recipe_image_endpoint(self.recipe.id)
        response = self.client.post(url, {"image": "notimage"}, format="multipart")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
