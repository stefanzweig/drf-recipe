from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Ingredient, Recipe

from recipe.serializers import IngredientSerializer


class IngredientApiTestCase(TestCase):
    """ Test the publicly available ingredients api """

    def setUp(self):
        self.ingredients_endpoint = reverse("recipe:ingredient-list")
        self.client = APIClient()

    def test_login_required(self):
        """ Test that login is required to access this endpoint """
        response = self.client.get(self.ingredients_endpoint)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class IngredientApiPrivateTestCase(TestCase):
    """ Test private ingredients api can be retrieved by authorized users """

    def setUp(self):
        self.ingredients_endpoint = reverse("recipe:ingredient-list")
        self.client = APIClient()
        self.email = "test@example.com"
        self.password = "testpass"
        self.user = self._create_user(email=self.email, password=self.password)
        self.client.force_authenticate(self.user)

    @staticmethod
    def _create_user(**kwargs):
        """ Create a sample user """
        return get_user_model().objects.create_user(**kwargs)

    @staticmethod
    def _create_ingredient(**kwargs):
        return Ingredient.objects.create(**kwargs)

    def test_get_ingredients(self):
        """ Test getting ingredients """
        self._create_ingredient(name="Kale", user=self.user)
        self._create_ingredient(name="Banana", user=self.user)

        response = self.client.get(self.ingredients_endpoint)

        ingredients = Ingredient.objects.all().order_by("-name")
        serializer = IngredientSerializer(ingredients, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_get_ingredients_limited_to_user(self):
        """ Test that only ingredients for authenticated users are returned """
        user2 = self._create_user(email="test2@example.com", password="testpass")
        ingredient1 = self._create_ingredient(name="Kale", user=self.user)
        self._create_ingredient(name="Banana", user=user2)

        response = self.client.get(self.ingredients_endpoint)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["name"], ingredient1.name)

    def test_create_ingredient_successful(self):
        """ Test create a new ingredient """
        payload = {"name": "Tomato"}
        self.client.post(self.ingredients_endpoint, payload)

        exists = Ingredient.objects.filter(name=payload["name"]).exists()

        self.assertTrue(exists)

    def test_create_ingredient_invalid_data(self):
        """ Test create a new ingredient with invalid data """
        payload = {"name": ""}
        response = self.client.post(self.ingredients_endpoint, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrive_ingredients_assigned_to_recipes(self):
        """ Test filtering ingredients by those assigned to recipes """
        ingredient1 = self._create_ingredient(user=self.user, name="Breakfast")
        ingredient2 = self._create_ingredient(user=self.user, name="Dinner")
        recipe = Recipe.objects.create(
            user=self.user, title="Cereal", time_in_minutes=50, price=4.9
        )
        recipe.ingredients.add(ingredient1)

        response = self.client.get(self.ingredients_endpoint, {"assigned_only": 1})

        serializer1 = IngredientSerializer(ingredient1)
        serializer2 = IngredientSerializer(ingredient2)
        self.assertIn(serializer1.data, response.data)
        self.assertNotIn(serializer2.data, response.data)

    def test_retrieve_ingredients_assigned_unique(self):
        """ Test filtering ingredients by assigned returns unique items """
        ingredient1 = self._create_ingredient(user=self.user, name="Breakfast")
        self._create_ingredient(user=self.user, name="Lunch")
        recipe1 = Recipe.objects.create(
            user=self.user, title="Cereal", time_in_minutes=50, price=4.9
        )
        recipe1.ingredients.add(ingredient1)
        recipe2 = Recipe.objects.create(
            user=self.user, title="Tea", time_in_minutes=50, price=4.9
        )
        recipe2.ingredients.add(ingredient1)

        response = self.client.get(self.ingredients_endpoint, {"assigned_only": 1})
        self.assertEqual(len(response.data), 1)
