from django.test import TestCase
from django.contrib.auth import get_user_model
from django.shortcuts import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core import models

from recipe.serializers import TagSerializer


class TagApiTestCase(TestCase):
    def setUp(self):
        self.tags_endpoint = reverse("recipe:tag-list")
        self.client = APIClient()

    def test_login_required(self):
        """ Test that login is required for retrieving tags """
        response = self.client.get(self.tags_endpoint)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TagApiPrivateTestCase(TestCase):
    def setUp(self):
        self.tags_endpoint = reverse("recipe:tag-list")
        self.client = APIClient()
        self.email = "test@example.com"
        self.password = "testpass"
        self.user = self._create_user(email=self.email, password=self.password)
        self.client.force_authenticate(self.user)
        self.tag = self._create_tag(user=self.user, name="Vegan")
        self.tag2 = self._create_tag(user=self.user, name="Vegetarian")

    def tearDown(self):
        # Delete tag
        self.tag.delete()
        self.tag2.delete()
        # Delete user
        self.user.delete()

    @staticmethod
    def _create_user(**kwargs):
        """ Create a sample user """
        return get_user_model().objects.create_user(**kwargs)

    @staticmethod
    def _create_tag(**kwargs):
        return models.Tag.objects.create(**kwargs)

    def test_get_tag(self):
        """ Test getting tags """
        response = self.client.get(self.tags_endpoint)
        tags = models.Tag.objects.all().order_by("-name")
        serializer = TagSerializer(tags, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_tag_filtered_by_user(self):
        """ Test that tags returned are filtered for authenticated user """
        self.user2 = self._create_user(
            email="test2@example.com", password=self.password
        )
        self.tag3 = self._create_tag(user=self.user2, name="Dessert")

        response = self.client.get(self.tags_endpoint)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]["name"], self.tag2.name)

        self.user2.delete()
        self.tag3.delete()

    def test_create_tag_successful(self):
        """ Test creating a new tag """
        payload = {"name": "Vegetables"}
        self.client.post(self.tags_endpoint, payload)

        exists = models.Tag.objects.filter(
            user=self.user, name=payload["name"]
        ).exists()

        self.assertTrue(exists)

    def test_create_tag_invalid(self):
        payload = {"name": ""}
        response = self.client.post(self.tags_endpoint, payload)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_tag_str(self):
        tag = models.Tag.objects.create(name="Cheese", user=self.user)
        self.assertEqual(str(tag), tag.name)

    def test_retrive_tags_assigned_to_recipes(self):
        """ Test filtering tags by those assigned to recipes """
        tag1 = self._create_tag(user=self.user, name="Breakfast")
        tag2 = self._create_tag(user=self.user, name="Dinner")
        recipe = models.Recipe.objects.create(
            user=self.user, title="Cereal", time_in_minutes=50, price=4.9
        )
        recipe.tags.add(tag1)

        response = self.client.get(self.tags_endpoint, {"assigned_only": 1})

        serializer1 = TagSerializer(tag1)
        serializer2 = TagSerializer(tag2)
        self.assertIn(serializer1.data, response.data)
        self.assertNotIn(serializer2.data, response.data)

    def test_retrieve_tags_assigned_unique(self):
        """ Test to make sure only unique tags with recipes attached are returned """
        tag = self._create_tag(user=self.user, name="Breakfast")
        self._create_tag(user=self.user, name="Lunch")
        recipe1 = models.Recipe.objects.create(
            user=self.user, title="Cereal", time_in_minutes=50, price=4.9
        )
        recipe1.tags.add(tag)
        recipe2 = models.Recipe.objects.create(
            user=self.user, title="Tea", time_in_minutes=50, price=4.9
        )
        recipe2.tags.add(tag)

        response = self.client.get(self.tags_endpoint, {"assigned_only": 1})
        self.assertEqual(len(response.data), 1)
