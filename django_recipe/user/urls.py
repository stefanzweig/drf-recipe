from django.conf.urls import url
from user import views

app_name = "user"

urlpatterns = [
    url(r"^create/$", views.CreateUserView.as_view(), name="create"),
    url(r"^token/$", views.CreateTokenView.as_view(), name="token"),
    url(r"^manage/$", views.ManageUserView.as_view(), name="manage"),
]
