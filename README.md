Django Recipe 
===
<p align="left">
	<a href="https://travis-ci.org/AaronKazah/django-recipe" alt="Contributors">
        <img src="https://travis-ci.org/AaronKazah/django-recipe.svg?branch=master" />
	</a>
	<a href='https://coveralls.io/github/AaronKazah/django-recipe?branch=HEAD'><img src='https://coveralls.io/repos/github/AaronKazah/django-recipe/badge.svg?branch=HEAD' alt='Coverage Status' /></a>
</p>
A recipe REST API built Python, DRF and Docker. The easiest way to run this app is 
by using Docker.

## Running with Docker
First build the image

```             
docker-compose -f docker-compose-dev.yml build
```

Then run the app 

``` 
docker-compose -f docker-compose-dev.yml up
```

Alternatively you can build AND run the app with this command

``` 
docker-compose -f docker-compose-dev.yml up --build
```

Docker will run the app on port 8000

## Local Development on Linux or Mac

1. Create a virtual environment for this app, or if you've installed `virtualenv` and `virtualenvwrapper` you can run
    ```.bash
    mkvirtualenv DJANGO_RECIPE --python='which python3'
    ```
2. Install your requirements
    ```.bash
    pip install -r requirements.txt
    ```
3. Set your environment variables from your shell
    ```.bash
    export SECRET_KEY=random_text_here
    ```
4. Create your database and run migrations
    ```.bash
    ./manage.py migrate
    ```
5. Run your dev server
    ```.bash
    ./manage.py runserver 0.0.0.0:8000
    ```

## Api Usage

Now let's walkthrough how to actually use this api, for this tutorial we'll be using [HTTPie](https://httpie.org/) to interact with our REST api.
You can also use whatever cli tool you want (for example `curl`)

#####  Creating a user and getting an auth token
Before you can start making any calls with the api, you need to create and authenticate a user. If you skip this step, you will get a HTTP 403 Forbidden error.
We'll assume you're using docker.

##### *Create user*
Create a user using the command below and enter any password after a prompt.

```.bash
docker-compose -f docker-compose-dev.yml run django /bin/bash -c "./manage.py createsuperuser --email test@example.com"
```

Now we have a user created! Normally you would acquire your token by posting your email and password to `/api/user/token` but for simplicity sake we'll generate one manually... so run

```.bash
docker-compose -f docker-compose-dev.yml run django /bin/bash -c "./manage.py drf_create_token test@example.com"
```

please copy the token return it should say:

```.bash
Generated token {token_id} for user test@example.com
```

and your done! Now we can begin making calls to our api

---

##### Get Ingredients
```.bash
http get http://127.0.0.1:8000/api/recipe/ingredients/ Authorization:'Token {token_id}'
```

##### Create an ingredient
```.bash
http post http://127.0.0.1:8000/api/recipe/ingredients/ Authorization:'Token {token_id}' name="Test Ingredient"
```

---

##### Get tags
```.bash
http get http://127.0.0.1:8000/api/recipe/tags/ Authorization:'Token {token_id}'
```

##### Create a tag
```.bash
http post http://127.0.0.1:8000/api/recipe/tags/ Authorization:'Token {token_id}' name="Test Tag"
```

---

##### GET recipes
```.bash
http get http://127.0.0.1:8000/api/recipe/recipes/ Authorization:'Token {token_id}'
```

##### GET recipes filtered by tags
```.bash
http get http://127.0.0.1:8000/api/recipe/recipes/?tag=test-tag Authorization:'Token {token_id}'
```

##### GET recipes filtered by ingredients
```.bash
http get http://127.0.0.1:8000/api/recipe/recipes/?ingredient=test-tag Authorization:'Token {token_id}'
```

##### Create recipe
```.bash
http post http://127.0.0.1:8000/api/recipe/recipes/ Authorization:'Token {token_id}' title=test time_in_minutes=10 price=49.00
```

##### GET a recipe
```.bash
http get http://127.0.0.1:8000/api/recipe/recipes/{id}/ Authorization:'Token {token_id}'
```

##### PATCH a recipe - let's add a new tag
```.bash
http patch http://127.0.0.1:8000/api/recipe/recipes/{id}/ Authorization:'Token {token_id}' tags:="[{tag_id}]"
```

##### PUT a recipe

```.bash
http PUT http://127.0.0.1:8000/api/recipe/recipes/{id}/ Authorization:'Token {token_id}' title=put_recipe time_in_minutes=12 price=22.00
```


## Testing
To run the test using Docker (recommended) run this command below
```.bash
docker-compose -f docker-compose-dev.yml run django -c "python manage.py test && flake8"
```
Alternatively you can run the tests in local development by running the command below. Please ensure you've exported your postgres variables in your shell/command line

```.bash
SECRET_KEY=
POSTGRES_HOST=
POSTGRES_NAME=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_PORT=
```
and then run
```.bash
python manage.py test && flake8
```


## Production

You can deploy this project to a production environment by running the normal `docker-compose` commands.

This file uses a `docker-compose.yml` to build an nginx-served django app with production based settings.
